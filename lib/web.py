from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep
from PIL import Image
import numpy as np
import os
from dotenv import dotenv_values

class Init:
    def __init__(self, debug, downPath):
        self.downPath = downPath
        self.debug = debug

    def Get(self, url, fileName):
        conf=dotenv_values(".env")
        chrome_options = Options()
        executable_path= conf['CHROME_DRIVER']
        mobileEmulation = {'deviceName': conf['MOBILE_DEVICE']}
        chrome_options.add_experimental_option('mobileEmulation', mobileEmulation)

        if self.debug == False:
            chrome_options.add_argument('--headless')  # 啟動Headless 無頭
            chrome_options.add_argument('--disable-gpu') #關閉GPU 避免某些系統或是網頁出錯

        driver = webdriver.Chrome(executable_path=executable_path,chrome_options=chrome_options)
        driver.get(url)
        sleep(float(conf['WAIT_WEB']))
        filePath = self.downPath + "/" + fileName
        window_height = driver.get_window_size()['height']
        page_height = driver.execute_script('return document.documentElement.scrollHeight')
        driver.get_screenshot_as_file(filePath + "_default.png")

        if page_height > window_height:
            n = page_height // window_height
            base_mat = np.atleast_2d(Image.open(filePath + "_default.png"))

            for i in range(n):
                driver.execute_script(f'document.documentElement.scrollTop={window_height*(i+1)};')
                sleep(float(conf['WAIT_IMAGE']))
                driver.save_screenshot(f'{filePath}_{i}.png')
                mat = np.atleast_2d(Image.open(f'{filePath}_{i}.png'))
                base_mat = np.append(base_mat, mat, axis=0)
            Image.fromarray(base_mat).save(filePath + '.png')
            for i in range(n):
                try:
                    os.remove(f'{filePath}_{i}.png')
                except OSError as e:
                    print(e)
            try:
                os.remove(filePath + "_default.png")
            except OSError as e:
                print(e)
        driver.quit()