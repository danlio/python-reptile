# python-reptile

爬蟲擷取網頁且做成圖片

## 必須要裝的
```
    pip3.7 install selenium
    pip3.7 install webdriver-manager
    pip3.7 install python-dotenv
    pip3.7 install Pillow
    pip3.7 install numpy
```

##
用 .map key by value 方式取針對 value 設定網頁抓取頁面

```
    google=https://news.google.com/topstories?hl=zh-TW&gl=TW&ceid=TW:zh-Hant
```


.env 裡面 executable_path 該變數是放 妳得 chromedriver

```
    #chrome 模擬手機型號
    MOBILE_DEVICE='iPhone SE'
    #chrome 驅動位置
    CHROME_DRIVER="chromedriver"
    #開啟網頁等待時間
    WAIT_WEB=2
    #擷取整頁圖分批時間
    WAIT_IMAGE=0.5
```
