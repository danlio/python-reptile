import pathlib
#路徑設定
mainPath = str(pathlib.Path().resolve())
import sys
#引用自己lib
sys.path.append(mainPath + '/lib')

#import file as Files
#讀取圖片路徑
#filelist = Files.Init(mainPath + '/image/*').list()
#print(filelist)

#爬蟲網頁
import web as Web

from dotenv import dotenv_values

UrlList =  dotenv_values(".map")

for fileName in UrlList:
    print(fileName, UrlList[fileName])
    Web.Init(True ,mainPath + '/image/').Get(UrlList[fileName], fileName)